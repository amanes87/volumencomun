package com.criptovolumen.entidades.vela.par;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coinapi.entidades.Timedata;
import com.criptovolumen.entidades.vela.Vela;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "VELA_BTC_5")
public class VelaBTC_5 extends Vela {

	public VelaBTC_5(Timedata td) {
		super(td);
	}

}
