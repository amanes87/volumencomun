package com.criptovolumen.entidades.vela.par;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coinapi.entidades.Timedata;
import com.criptovolumen.entidades.vela.Vela;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "VELA_BTC_120")
public class VelaBTC_120 extends Vela {

	public VelaBTC_120(Timedata td) {
		super(td);
	}
	
	public VelaBTC_120(VelaBTC_60 vela) {
		super(vela);
	}

	public void cerrarVela(VelaBTC_60 vela) {
		if(getPrice_high() < vela.getPrice_high()) {
			setPrice_high(vela.getPrice_high());
		}
		if(getPrice_low() > vela.getPrice_low()) {
			setPrice_low(vela.getPrice_low());
		}
		setPrice_close(vela.getPrice_close());
		setTime_close(vela.getTime_close());
		setTime_period_end(vela.getTime_period_end());
		setVolume_traded(getVolume_traded() + vela.getVolume_traded());
		setTrades_count(getTrades_count() + vela.getTrades_count());
	}
	
	public VelaBTC_120(VelaBTC_15 vela) {
		super(vela);
	}

	public void cerrarVela(VelaBTC_15 vela) {
		if(getPrice_high() < vela.getPrice_high()) {
			setPrice_high(vela.getPrice_high());
		}
		if(getPrice_low() > vela.getPrice_low()) {
			setPrice_low(vela.getPrice_low());
		}
		setPrice_close(vela.getPrice_close());
		setTime_close(vela.getTime_close());
		setTime_period_end(vela.getTime_period_end());
		setVolume_traded(getVolume_traded() + vela.getVolume_traded());
		setTrades_count(getTrades_count() + vela.getTrades_count());
	}

}
