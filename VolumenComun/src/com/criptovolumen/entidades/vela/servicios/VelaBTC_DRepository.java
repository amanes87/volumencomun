package com.criptovolumen.entidades.vela.servicios;

import java.time.Instant;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.criptovolumen.entidades.vela.par.VelaBTC_D;

public interface VelaBTC_DRepository extends CrudRepository<VelaBTC_D, Long>{
	
	@Query(value = "SELECT v FROM VelaBTC_D v WHERE v.time_period_start >= ?1 AND v.time_period_end <= ?2 order by time_period_start asc")
	List<VelaBTC_D> findVelasBetweenDatesSortedAsc(Instant timePeriodStart, Instant timePeriodEnd);
}
