package com.criptovolumen.entidades.vela.servicios;

import java.time.Instant;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.criptovolumen.entidades.vela.par.VelaBTC_15;

public interface VelaBTC_15Repository extends CrudRepository<VelaBTC_15, Long>{

	@Query(nativeQuery = true, value = "select * from VELA_BTC_15 order by time_period_start desc  limit ?1 offset 0")
	public List<VelaBTC_15> ultimosRegistros(int limit);
	
	@Query(nativeQuery = true, value = "select * from VELA_BTC_15 order by time_period_start asc")
	public List<VelaBTC_15> todosRegistrosAscendiente();
	
	@Query(value = "SELECT v FROM VelaBTC_15 v WHERE v.time_period_start >= ?1 AND v.time_period_end <= ?2 order by time_period_start asc")
	List<VelaBTC_15> findVelasBetweenDatesSortedAsc(Instant timePeriodStart, Instant timePeriodEnd);

	@Query(nativeQuery = true, value = "select MIN(time_period_start) from VELA_BTC_15")
	public Instant obtenerPeriodoMasAntiguoInformado();
}
