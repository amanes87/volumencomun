package com.criptovolumen.entidades.vela.servicios;

import org.springframework.data.repository.CrudRepository;

import com.criptovolumen.entidades.vela.par.VelaBTC_W;

public interface VelaBTC_WRepository extends CrudRepository<VelaBTC_W, Long>{

}
