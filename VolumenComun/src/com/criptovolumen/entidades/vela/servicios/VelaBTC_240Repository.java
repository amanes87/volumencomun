package com.criptovolumen.entidades.vela.servicios;

import java.time.Instant;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.criptovolumen.entidades.vela.par.VelaBTC_240;

public interface VelaBTC_240Repository extends CrudRepository<VelaBTC_240, Long>{

	@Query(nativeQuery = true, value = "select * from VELA_BTC_240 order by time_period_start desc  limit ?1 offset 0")
	public List<VelaBTC_240> ultimosRegistros(int limit);

	@Query(nativeQuery = true, value = "select * from VELA_BTC_240 order by time_period_start asc")
	public List<VelaBTC_240> todosRegistrosAscendiente();	

	@Query(value = "SELECT v FROM VelaBTC_240 v WHERE v.time_period_start >= ?1 AND v.time_period_end <= ?2 order by time_period_start asc")
	List<VelaBTC_240> findVelasBetweenDatesSortedAsc(Instant timePeriodStart, Instant timePeriodEnd);
}
