package com.criptovolumen.entidades.vela.servicios;

import org.springframework.data.repository.CrudRepository;

import com.criptovolumen.entidades.vela.par.VelaBTC_M;

public interface VelaBTC_MRepository extends CrudRepository<VelaBTC_M, Long>{

}
