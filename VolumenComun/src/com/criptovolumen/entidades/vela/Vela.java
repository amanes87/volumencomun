package com.criptovolumen.entidades.vela;


import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.coinapi.entidades.Timedata;
import com.criptovolumen.entidades.vela.par.VelaBTC_120;
import com.criptovolumen.entidades.vela.par.VelaBTC_15;
import com.criptovolumen.entidades.vela.par.VelaBTC_240;
import com.criptovolumen.entidades.vela.par.VelaBTC_30;
import com.criptovolumen.entidades.vela.par.VelaBTC_60;
import com.criptovolumen.entidades.vela.par.VelaBTC_720;
import com.criptovolumen.entidades.vela.par.VelaBTC_D;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class Vela {

	@Id
	private Instant time_period_start; // Period starting time (range left inclusive)
	private Instant time_period_end; // Period ending time (range right exclusive)
	private Instant time_open; // Time of first trade inside period range
	private Instant time_close; // Time of last trade inside period range
	private double price_open; // First trade price inside period range
	private double price_high; // Highest traded price inside period range
	private double price_low; // Lowest traded price inside period range
	private double price_close; // Last trade price inside period range
	private double volume_traded; // Cumulative base amount traded inside period range
	private int trades_count; // Amount of trades executed inside period range
	
	public Vela(Timedata timeDataCoinApi) {
		this.time_period_start = timeDataCoinApi.get_time_period_start();
		this.time_period_end = timeDataCoinApi.get_time_period_end();
		this.time_open = timeDataCoinApi.get_time_open();
		this.time_close = timeDataCoinApi.get_time_close();
		this.price_open = timeDataCoinApi.get_price_open();
		this.price_high = timeDataCoinApi.get_price_high();
		this.price_low = timeDataCoinApi.get_price_low();
		this.price_close = timeDataCoinApi.get_price_close();
		this.volume_traded = timeDataCoinApi.get_volume_traded();
		this.trades_count = timeDataCoinApi.get_trades_count();
	}
	
	public Vela(Vela vela) {
		this.time_period_start = vela.getTime_period_start();
		this.time_period_end = vela.getTime_period_end();
		this.time_open = vela.getTime_open();
		this.time_close = vela.getTime_close();
		this.price_open = vela.getPrice_open();
		this.price_high = vela.getPrice_high();
		this.price_low = vela.getPrice_low();
		this.price_close = vela.getPrice_close();
		this.volume_traded = vela.getVolume_traded();
		this.trades_count = vela.getTrades_count();
	}

	@Override
	public String toString() {
		return "Vela [time_period_start=" + time_period_start + ", time_period_end=" + time_period_end + ", time_open="
				+ time_open + ", time_close=" + time_close + ", price_open=" + price_open + ", price_high=" + price_high
				+ ", price_low=" + price_low + ", price_close=" + price_close + ", volume_traded=" + volume_traded
				+ ", trades_count=" + trades_count + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(price_close);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(price_high);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(price_low);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(price_open);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((time_close == null) ? 0 : time_close.hashCode());
		result = prime * result + ((time_open == null) ? 0 : time_open.hashCode());
		result = prime * result + ((time_period_end == null) ? 0 : time_period_end.hashCode());
		result = prime * result + ((time_period_start == null) ? 0 : time_period_start.hashCode());
		result = prime * result + trades_count;
		temp = Double.doubleToLongBits(volume_traded);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vela other = (Vela) obj;
		if (Double.doubleToLongBits(price_close) != Double.doubleToLongBits(other.price_close))
			return false;
		if (Double.doubleToLongBits(price_high) != Double.doubleToLongBits(other.price_high))
			return false;
		if (Double.doubleToLongBits(price_low) != Double.doubleToLongBits(other.price_low))
			return false;
		if (Double.doubleToLongBits(price_open) != Double.doubleToLongBits(other.price_open))
			return false;
		if (time_close == null) {
			if (other.time_close != null)
				return false;
		} else if (!time_close.equals(other.time_close))
			return false;
		if (time_open == null) {
			if (other.time_open != null)
				return false;
		} else if (!time_open.equals(other.time_open))
			return false;
		if (time_period_end == null) {
			if (other.time_period_end != null)
				return false;
		} else if (!time_period_end.equals(other.time_period_end))
			return false;
		if (time_period_start == null) {
			if (other.time_period_start != null)
				return false;
		} else if (!time_period_start.equals(other.time_period_start))
			return false;
		if (trades_count != other.trades_count)
			return false;
		if (Double.doubleToLongBits(volume_traded) != Double.doubleToLongBits(other.volume_traded))
			return false;
		return true;
	}
	
	public LocalDateTime getTimePeriodStartLocalDateTime() {
		return LocalDateTime.ofInstant(this.time_period_start, ZoneOffset.UTC);
	}
	
	public LocalDateTime getTimePeriodEndLocalDateTime() {
		return LocalDateTime.ofInstant(this.time_period_end, ZoneOffset.UTC);
	}
	
	public Vela(VelaBTC_15 vela) {
		this.time_period_start = vela.getTime_period_start();
		this.time_period_end = vela.getTime_period_end();
		this.time_open = vela.getTime_open();
		this.time_close = vela.getTime_close();
		this.price_open = vela.getPrice_open();
		this.price_high = vela.getPrice_high();
		this.price_low = vela.getPrice_low();
		this.price_close = vela.getPrice_close();
		this.volume_traded = vela.getVolume_traded();
		this.trades_count = vela.getTrades_count();
	}
	
	public Vela(VelaBTC_30 vela) {
		this.time_period_start = vela.getTime_period_start();
		this.time_period_end = vela.getTime_period_end();
		this.time_open = vela.getTime_open();
		this.time_close = vela.getTime_close();
		this.price_open = vela.getPrice_open();
		this.price_high = vela.getPrice_high();
		this.price_low = vela.getPrice_low();
		this.price_close = vela.getPrice_close();
		this.volume_traded = vela.getVolume_traded();
		this.trades_count = vela.getTrades_count();
	}
	
	public Vela(VelaBTC_60 vela) {
		this.time_period_start = vela.getTime_period_start();
		this.time_period_end = vela.getTime_period_end();
		this.time_open = vela.getTime_open();
		this.time_close = vela.getTime_close();
		this.price_open = vela.getPrice_open();
		this.price_high = vela.getPrice_high();
		this.price_low = vela.getPrice_low();
		this.price_close = vela.getPrice_close();
		this.volume_traded = vela.getVolume_traded();
		this.trades_count = vela.getTrades_count();
	}
	
	public Vela(VelaBTC_120 vela) {
		this.time_period_start = vela.getTime_period_start();
		this.time_period_end = vela.getTime_period_end();
		this.time_open = vela.getTime_open();
		this.time_close = vela.getTime_close();
		this.price_open = vela.getPrice_open();
		this.price_high = vela.getPrice_high();
		this.price_low = vela.getPrice_low();
		this.price_close = vela.getPrice_close();
		this.volume_traded = vela.getVolume_traded();
		this.trades_count = vela.getTrades_count();
	}
	
	public Vela(VelaBTC_240 vela) {
		this.time_period_start = vela.getTime_period_start();
		this.time_period_end = vela.getTime_period_end();
		this.time_open = vela.getTime_open();
		this.time_close = vela.getTime_close();
		this.price_open = vela.getPrice_open();
		this.price_high = vela.getPrice_high();
		this.price_low = vela.getPrice_low();
		this.price_close = vela.getPrice_close();
		this.volume_traded = vela.getVolume_traded();
		this.trades_count = vela.getTrades_count();
	}
	
	public Vela(VelaBTC_720 vela) {
		this.time_period_start = vela.getTime_period_start();
		this.time_period_end = vela.getTime_period_end();
		this.time_open = vela.getTime_open();
		this.time_close = vela.getTime_close();
		this.price_open = vela.getPrice_open();
		this.price_high = vela.getPrice_high();
		this.price_low = vela.getPrice_low();
		this.price_close = vela.getPrice_close();
		this.volume_traded = vela.getVolume_traded();
		this.trades_count = vela.getTrades_count();
	}
	
	public Vela(VelaBTC_D vela) {
		this.time_period_start = vela.getTime_period_start();
		this.time_period_end = vela.getTime_period_end();
		this.time_open = vela.getTime_open();
		this.time_close = vela.getTime_close();
		this.price_open = vela.getPrice_open();
		this.price_high = vela.getPrice_high();
		this.price_low = vela.getPrice_low();
		this.price_close = vela.getPrice_close();
		this.volume_traded = vela.getVolume_traded();
		this.trades_count = vela.getTrades_count();
	}
	
}
