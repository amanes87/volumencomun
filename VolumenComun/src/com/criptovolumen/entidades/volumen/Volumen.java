package com.criptovolumen.entidades.volumen;

import java.time.Instant;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.coinapi.entidades.Timedata;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public abstract class Volumen {

	@Id
	private Instant time_period_start; // Period starting time (range left inclusive)
	private Instant time_period_end; // Period ending time (range right exclusive)
	private double volume_traded; // Cumulative base amount traded inside period range
	
	public Volumen(Timedata timeDataCoinApi) {
		this.time_period_start = timeDataCoinApi.get_time_period_start();
		this.time_period_end = timeDataCoinApi.get_time_period_end();
	}
	
}
