package com.criptovolumen.entidades.volumen.servicios;

import java.time.Instant;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.criptovolumen.entidades.volumen.par.VolumenBTC;

public interface VolumenBTCRepository extends CrudRepository<VolumenBTC, Long>{

	@Query(value = "SELECT v FROM VolumenBTC v")
	List<VolumenBTC> findAllVolumenBTC(Sort sort);
	
	@Query(value = "SELECT new VolumenBTC(v.time_period_start, v.time_period_end, v.volume_traded) FROM VolumenBTC v")
	List<VolumenBTC> findAllSimpleVolumenBTC();

	@Query(value = "SELECT v FROM VolumenBTC v WHERE v.time_period_start = ?1")
	VolumenBTC findVolumenBTCByTimePeriodStart(Instant timePeriodStart);
	
	@Query(value = "SELECT new VolumenBTC(v.time_period_start, v.time_period_end, v.volume_traded) FROM VolumenBTC v WHERE v.time_period_start >= ?1 AND v.time_period_start <= ?2")
	List<VolumenBTC> findAllSimpleVolumenBTCBetweenDates(Instant timePeriodStartInicio, Instant timePeriodStartFinal);
}
