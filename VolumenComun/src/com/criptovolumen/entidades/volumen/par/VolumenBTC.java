package com.criptovolumen.entidades.volumen.par;

import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coinapi.entidades.Timedata;
import com.criptovolumen.entidades.volumen.Volumen;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "VOLUMEN_BTC")
public class VolumenBTC extends Volumen {
	
	//AÑADIDO A MANO
	private Double coinbeneSpotBtcUSDT;

	private Double coinbeneSpotBtcUSDTMin;

	private Double coinbeneSpotBtcUSDTMax;
	//FIN AÑADIDO A MANO

	
	private Double okcoinCnySpotBtcCny;

	private Double okcoinCnySpotBtcCnyMin;

	private Double okcoinCnySpotBtcCnyMax;

	private Double huobiSpotBtcCny;

	private Double huobiSpotBtcCnyMin;

	private Double huobiSpotBtcCnyMax;

	private Double btcchinaSpotBtcCny;

	private Double btcchinaSpotBtcCnyMin;

	private Double btcchinaSpotBtcCnyMax;

	private Double coincheckSpotBtcJpy;

	private Double coincheckSpotBtcJpyMin;

	private Double coincheckSpotBtcJpyMax;

	private Double bitfinexSpotBtcUsd;

	private Double bitfinexSpotBtcUsdMin;

	private Double bitfinexSpotBtcUsdMax;

	private Double coinbaseSpotBtcUsd;

	private Double coinbaseSpotBtcUsdMin;

	private Double coinbaseSpotBtcUsdMax;

	private Double btcESpotBtcUsd;

	private Double btcESpotBtcUsdMin;

	private Double btcESpotBtcUsdMax;

	private Double binanceSpotBtcUsdt;

	private Double binanceSpotBtcUsdtMin;

	private Double binanceSpotBtcUsdtMax;

	private Double chbtcSpotBtcCny;

	private Double chbtcSpotBtcCnyMin;

	private Double chbtcSpotBtcCnyMax;

	private Double bitstampSpotBtcUsd;

	private Double bitstampSpotBtcUsdMin;

	private Double bitstampSpotBtcUsdMax;

	private Double krakenSpotBtcEur;

	private Double krakenSpotBtcEurMin;

	private Double krakenSpotBtcEurMax;

	private Double bithumbSpotBtcKrw;

	private Double bithumbSpotBtcKrwMin;

	private Double bithumbSpotBtcKrwMax;

	private Double bitflyerSpotBtcJpy;

	private Double bitflyerSpotBtcJpyMin;

	private Double bitflyerSpotBtcJpyMax;

	private Double poloniexSpotBtcUsdt;

	private Double poloniexSpotBtcUsdtMin;

	private Double poloniexSpotBtcUsdtMax;

	private Double btctradeSpotBtcCny;

	private Double btctradeSpotBtcCnyMin;

	private Double btctradeSpotBtcCnyMax;

	private Double coinbaseSpotBtcEur;

	private Double coinbaseSpotBtcEurMin;

	private Double coinbaseSpotBtcEurMax;

	private Double coinoneSpotBtcKrw;

	private Double coinoneSpotBtcKrwMin;

	private Double coinoneSpotBtcKrwMax;

	private Double okcoinUsdSpotBtcUsd;

	private Double okcoinUsdSpotBtcUsdMin;

	private Double okcoinUsdSpotBtcUsdMax;

	private Double krakenSpotBtcUsd;

	private Double krakenSpotBtcUsdMin;

	private Double krakenSpotBtcUsdMax;

	private Double zaifSpotBtcJpy;

	private Double zaifSpotBtcJpyMin;

	private Double zaifSpotBtcJpyMax;

	private Double mtgoxSpotBtcUsd;

	private Double mtgoxSpotBtcUsdMin;

	private Double mtgoxSpotBtcUsdMax;

	private Double bittrexSpotBtcUsdt;

	private Double bittrexSpotBtcUsdtMin;

	private Double bittrexSpotBtcUsdtMax;

	private Double huobiproSpotBtcUsdt;

	private Double huobiproSpotBtcUsdtMin;

	private Double huobiproSpotBtcUsdtMax;

	private Double lakebtcSpotBtcUsd;

	private Double lakebtcSpotBtcUsdMin;

	private Double lakebtcSpotBtcUsdMax;

	private Double bitstampSpotBtcEur;

	private Double bitstampSpotBtcEurMin;

	private Double bitstampSpotBtcEurMax;

	private Double korbitSpotBtcKrw;

	private Double korbitSpotBtcKrwMin;

	private Double korbitSpotBtcKrwMax;

	private Double geminiSpotBtcUsd;

	private Double geminiSpotBtcUsdMin;

	private Double geminiSpotBtcUsdMax;

	private Double hitbtcSpotBtcUsdt;

	private Double hitbtcSpotBtcUsdtMin;

	private Double hitbtcSpotBtcUsdtMax;

	private Double exmoSpotBtcUsd;

	private Double exmoSpotBtcUsdMin;

	private Double exmoSpotBtcUsdMax;

	private Double acxSpotBtcAud;

	private Double acxSpotBtcAudMin;

	private Double acxSpotBtcAudMax;

	private Double coinbaseSpotBtcGbp;

	private Double coinbaseSpotBtcGbpMin;

	private Double coinbaseSpotBtcGbpMax;

	private Double okexSpotBtcUsdt;

	private Double okexSpotBtcUsdtMin;

	private Double okexSpotBtcUsdtMax;

	private Double exmoSpotBtcRub;

	private Double exmoSpotBtcRubMin;

	private Double exmoSpotBtcRubMax;

	private Double itbitSpotBtcUsd;

	private Double itbitSpotBtcUsdMin;

	private Double itbitSpotBtcUsdMax;

	private Double mixcoinsSpotBtcUsd;

	private Double mixcoinsSpotBtcUsdMin;

	private Double mixcoinsSpotBtcUsdMax;

	private Double virwoxSpotBtcSll;

	private Double virwoxSpotBtcSllMin;

	private Double virwoxSpotBtcSllMax;

	private Double mercadobitcoinSpotBtcBrl;

	private Double mercadobitcoinSpotBtcBrlMin;

	private Double mercadobitcoinSpotBtcBrlMax;

	private Double btcESpotBtcRub;

	private Double btcESpotBtcRubMin;

	private Double btcESpotBtcRubMax;

	private Double bitmarketSpotBtcPln;

	private Double bitmarketSpotBtcPlnMin;

	private Double bitmarketSpotBtcPlnMax;

	private Double liquiSpotBtcUsdt;

	private Double liquiSpotBtcUsdtMin;

	private Double liquiSpotBtcUsdtMax;

	private Double bitcoinidSpotBtcIdr;

	private Double bitcoinidSpotBtcIdrMin;

	private Double bitcoinidSpotBtcIdrMax;

	private Double bitsoSpotBtcMxn;

	private Double bitsoSpotBtcMxnMin;

	private Double bitsoSpotBtcMxnMax;

	private Double btcboxSpotBtcJpy;

	private Double btcboxSpotBtcJpyMin;

	private Double btcboxSpotBtcJpyMax;

	private Double cexioSpotBtcUsd;

	private Double cexioSpotBtcUsdMin;

	private Double cexioSpotBtcUsdMax;

	private Double quoineSpotBtcJpy;

	private Double quoineSpotBtcJpyMin;

	private Double quoineSpotBtcJpyMax;

	private Double lunoSpotBtcZar;

	private Double lunoSpotBtcZarMin;

	private Double lunoSpotBtcZarMax;

	private Double therocktradingSpotBtcEur;

	private Double therocktradingSpotBtcEurMin;

	private Double therocktradingSpotBtcEurMax;

	private Double cryptopiaSpotBtcUsdt;

	private Double cryptopiaSpotBtcUsdtMin;

	private Double cryptopiaSpotBtcUsdtMax;

	private Double bitfinexSpotBtcEur;

	private Double bitfinexSpotBtcEurMin;

	private Double bitfinexSpotBtcEurMax;

	private Double btcESpotBtcEur;

	private Double btcESpotBtcEurMin;

	private Double btcESpotBtcEurMax;

	private Double wexnzSpotBtcUsd;

	private Double wexnzSpotBtcUsdMin;

	private Double wexnzSpotBtcUsdMax;

	private Double quadrigacxSpotBtcCad;

	private Double quadrigacxSpotBtcCadMin;

	private Double quadrigacxSpotBtcCadMax;

	private Double mtgoxSpotBtcEur;

	private Double mtgoxSpotBtcEurMin;

	private Double mtgoxSpotBtcEurMax;

	private Double lakebtcSpotBtcJpy;

	private Double lakebtcSpotBtcJpyMin;

	private Double lakebtcSpotBtcJpyMax;

	private Double gateioSpotBtcUsdt;

	private Double gateioSpotBtcUsdtMin;

	private Double gateioSpotBtcUsdtMax;

	private Double quoineSpotBtcUsd;

	private Double quoineSpotBtcUsdMin;

	private Double quoineSpotBtcUsdMax;

	private Double exmoSpotBtcEur;

	private Double exmoSpotBtcEurMin;

	private Double exmoSpotBtcEurMax;

	private Double exmoSpotBtcUah;

	private Double exmoSpotBtcUahMin;

	private Double exmoSpotBtcUahMax;

	private Double lakebtcSpotBtcEur;

	private Double lakebtcSpotBtcEurMin;

	private Double lakebtcSpotBtcEurMax;

	private Double lakebtcSpotBtcCad;

	private Double lakebtcSpotBtcCadMin;

	private Double lakebtcSpotBtcCadMax;

	private Double coinfloorSpotBtcGbp;

	private Double coinfloorSpotBtcGbpMin;

	private Double coinfloorSpotBtcGbpMax;

	private Double hitbtcSpotBtcEur;

	private Double hitbtcSpotBtcEurMin;

	private Double hitbtcSpotBtcEurMax;

	private Double livecoinSpotBtcUsd;

	private Double livecoinSpotBtcUsdMin;

	private Double livecoinSpotBtcUsdMax;

	private Double yobitSpotBtcRub;

	private Double yobitSpotBtcRubMin;

	private Double yobitSpotBtcRubMax;

	private Double yobitSpotBtcUsd;

	private Double yobitSpotBtcUsdMin;

	private Double yobitSpotBtcUsdMax;

	private Double bterSpotBtcCny;

	private Double bterSpotBtcCnyMin;

	private Double bterSpotBtcCnyMax;

	private Double lakebtcSpotBtcGbp;

	private Double lakebtcSpotBtcGbpMin;

	private Double lakebtcSpotBtcGbpMax;

	private Double coinmateSpotBtcEur;

	private Double coinmateSpotBtcEurMin;

	private Double coinmateSpotBtcEurMax;

	private Double coinmateSpotBtcCzk;

	private Double coinmateSpotBtcCzkMin;

	private Double coinmateSpotBtcCzkMax;

	private Double btcmarketsSpotBtcAud;

	private Double btcmarketsSpotBtcAudMin;

	private Double btcmarketsSpotBtcAudMax;

	private Double krakenSpotBtcJpy;

	private Double krakenSpotBtcJpyMin;

	private Double krakenSpotBtcJpyMax;

	private Double krakenSpotBtcCad;

	private Double krakenSpotBtcCadMin;

	private Double krakenSpotBtcCadMax;

	private Double btcturkSpotBtcTry;

	private Double btcturkSpotBtcTryMin;

	private Double btcturkSpotBtcTryMax;

	private Double mtgoxSpotBtcGbp;

	private Double mtgoxSpotBtcGbpMin;

	private Double mtgoxSpotBtcGbpMax;

	private Double bxinthSpotBtcThb;

	private Double bxinthSpotBtcThbMin;

	private Double bxinthSpotBtcThbMax;

	private Double lunoSpotBtcNgn;

	private Double lunoSpotBtcNgnMin;

	private Double lunoSpotBtcNgnMax;

	private Double exmoSpotBtcUsdt;

	private Double exmoSpotBtcUsdtMin;

	private Double exmoSpotBtcUsdtMax;

	private Double gatecoinSpotBtcUsd;

	private Double gatecoinSpotBtcUsdMin;

	private Double gatecoinSpotBtcUsdMax;

	private Double wexnzSpotBtcRub;

	private Double wexnzSpotBtcRubMin;

	private Double wexnzSpotBtcRubMax;

	private Double itbitSpotBtcEur;

	private Double itbitSpotBtcEurMin;

	private Double itbitSpotBtcEurMax;

	private Double independentreserveSpotBtcAud;

	private Double independentreserveSpotBtcAudMin;

	private Double independentreserveSpotBtcAudMax;

	private Double quoineSpotBtcSgd;

	private Double quoineSpotBtcSgdMin;

	private Double quoineSpotBtcSgdMax;

	private Double gatecoinSpotBtcHkd;

	private Double gatecoinSpotBtcHkdMin;

	private Double gatecoinSpotBtcHkdMax;

	private Double kucoinSpotBtcUsdt;

	private Double kucoinSpotBtcUsdtMin;

	private Double kucoinSpotBtcUsdtMax;

	private Double gatecoinSpotBtcEur;

	private Double gatecoinSpotBtcEurMin;

	private Double gatecoinSpotBtcEurMax;

	private Double lunoSpotBtcMyr;

	private Double lunoSpotBtcMyrMin;

	private Double lunoSpotBtcMyrMax;

	private Double poloniexSpotMyrBtc;

	private Double poloniexSpotMyrBtcMin;

	private Double poloniexSpotMyrBtcMax;

	private Double mtgoxSpotBtcJpy;

	private Double mtgoxSpotBtcJpyMin;

	private Double mtgoxSpotBtcJpyMax;

	private Double cexioSpotBtcEur;

	private Double cexioSpotBtcEurMin;

	private Double cexioSpotBtcEurMax;

	private Double dsxSpotBtcUsd;

	private Double dsxSpotBtcUsdMin;

	private Double dsxSpotBtcUsdMax;

	private Double krakenSpotBtcGbp;

	private Double krakenSpotBtcGbpMin;

	private Double krakenSpotBtcGbpMax;

	private Double wexnzSpotBtcEur;

	private Double wexnzSpotBtcEurMin;

	private Double wexnzSpotBtcEurMax;

	private Double coinbaseSpotBtcCad;

	private Double coinbaseSpotBtcCadMin;

	private Double coinbaseSpotBtcCadMax;

	private Double ccexSpotUsdBtc;

	private Double ccexSpotUsdBtcMin;

	private Double ccexSpotUsdBtcMax;

	private Double therocktradingSpotBtcUsd;

	private Double therocktradingSpotBtcUsdMin;

	private Double therocktradingSpotBtcUsdMax;

	private Double itbitSpotBtcSgd;

	private Double itbitSpotBtcSgdMin;

	private Double itbitSpotBtcSgdMax;

	private Double mtgoxSpotBtcAud;

	private Double mtgoxSpotBtcAudMin;

	private Double mtgoxSpotBtcAudMax;

	private Double coinnestSpotBtcKrw;

	private Double coinnestSpotBtcKrwMin;

	private Double coinnestSpotBtcKrwMax;

	private Double tidexSpotBtcUsdt;

	private Double tidexSpotBtcUsdtMin;

	private Double tidexSpotBtcUsdtMax;

	private Double mtgoxSpotBtcPln;

	private Double mtgoxSpotBtcPlnMin;

	private Double mtgoxSpotBtcPlnMax;

	private Double btcxSpotBtcUsd;

	private Double btcxSpotBtcUsdMin;

	private Double btcxSpotBtcUsdMax;

	private Double bittrexSpotMyrBtc;

	private Double bittrexSpotMyrBtcMin;

	private Double bittrexSpotMyrBtcMax;

	private Double lakebtcSpotBtcAud;

	private Double lakebtcSpotBtcAudMin;

	private Double lakebtcSpotBtcAudMax;

	private Double lakebtcSpotBtcSgd;

	private Double lakebtcSpotBtcSgdMin;

	private Double lakebtcSpotBtcSgdMax;

	private Double quoineSpotBtcEur;

	private Double quoineSpotBtcEurMin;

	private Double quoineSpotBtcEurMax;

	private Double quoineSpotBtcAud;

	private Double quoineSpotBtcAudMin;

	private Double quoineSpotBtcAudMax;

	private Double bittrexSpotTusdBtc;

	private Double bittrexSpotTusdBtcMin;

	private Double bittrexSpotTusdBtcMax;

	private Double btcxSpotBtcEur;

	private Double btcxSpotBtcEurMin;

	private Double btcxSpotBtcEurMax;

	private Double kunaSpotBtcUah;

	private Double kunaSpotBtcUahMin;

	private Double kunaSpotBtcUahMax;

	private Double dsxSpotBtcEur;

	private Double dsxSpotBtcEurMin;

	private Double dsxSpotBtcEurMax;

	private Double lunoSpotBtcIdr;

	private Double lunoSpotBtcIdrMin;

	private Double lunoSpotBtcIdrMax;

	private Double bitmarketSpotBtcEur;

	private Double bitmarketSpotBtcEurMin;

	private Double bitmarketSpotBtcEurMax;

	private Double btctradeuaSpotBtcUah;

	private Double btctradeuaSpotBtcUahMin;

	private Double btctradeuaSpotBtcUahMax;

	private Double quadrigacxSpotBtcUsd;

	private Double quadrigacxSpotBtcUsdMin;

	private Double quadrigacxSpotBtcUsdMax;

	private Double abucoinsSpotBtcPln;

	private Double abucoinsSpotBtcPlnMin;

	private Double abucoinsSpotBtcPlnMax;

	private Double fybsgSpotBtcSgd;

	private Double fybsgSpotBtcSgdMin;

	private Double fybsgSpotBtcSgdMax;

	private Double cryptopiaSpotTopBtc;

	private Double cryptopiaSpotTopBtcMin;

	private Double cryptopiaSpotTopBtcMax;

	private Double quoineSpotBtcHkd;

	private Double quoineSpotBtcHkdMin;

	private Double quoineSpotBtcHkdMax;

	private Double quoineSpotBtcPhp;

	private Double quoineSpotBtcPhpMin;

	private Double quoineSpotBtcPhpMax;

	private Double quoineSpotBtcIdr;

	private Double quoineSpotBtcIdrMin;

	private Double quoineSpotBtcIdrMax;

	private Double livecoinSpotBtcRub;

	private Double livecoinSpotBtcRubMin;

	private Double livecoinSpotBtcRubMax;

	private Double bitkonanSpotBtcUsd;

	private Double bitkonanSpotBtcUsdMin;

	private Double bitkonanSpotBtcUsdMax;

	private Double fybsgSpotBtcSek;

	private Double fybsgSpotBtcSekMin;

	private Double fybsgSpotBtcSekMax;

	private Double exmoSpotBtcPln;

	private Double exmoSpotBtcPlnMin;

	private Double exmoSpotBtcPlnMax;

	private Double mtgoxSpotBtcCad;

	private Double mtgoxSpotBtcCadMin;

	private Double mtgoxSpotBtcCadMax;

	private Double livecoinSpotBtcEur;

	private Double livecoinSpotBtcEurMin;

	private Double livecoinSpotBtcEurMax;

	private Double braziliexSpotBtcBrl;

	private Double braziliexSpotBtcBrlMin;

	private Double braziliexSpotBtcBrlMax;

	private Double cexioSpotBtcGbp;

	private Double cexioSpotBtcGbpMin;

	private Double cexioSpotBtcGbpMax;

	private Double coinfloorSpotBtcUsd;

	private Double coinfloorSpotBtcUsdMin;

	private Double coinfloorSpotBtcUsdMax;

	private Double fybseSpotBtcSek;

	private Double fybseSpotBtcSekMin;

	private Double fybseSpotBtcSekMax;

	private Double independentreserveSpotBtcNzd;

	private Double independentreserveSpotBtcNzdMin;

	private Double independentreserveSpotBtcNzdMax;

	private Double btccSpotBtcUsd;

	private Double btccSpotBtcUsdMin;

	private Double btccSpotBtcUsdMax;

	private Double abucoinsSpotBtcEur;

	private Double abucoinsSpotBtcEurMin;

	private Double abucoinsSpotBtcEurMax;

	private Double btcESpotBtcGbp;

	private Double btcESpotBtcGbpMin;

	private Double btcESpotBtcGbpMax;

	private Double independentreserveSpotBtcUsd;

	private Double independentreserveSpotBtcUsdMin;

	private Double independentreserveSpotBtcUsdMax;

	private Double cexioSpotBtcRub;

	private Double cexioSpotBtcRubMin;

	private Double cexioSpotBtcRubMax;

	private Double btcESpotBtcCny;

	private Double btcESpotBtcCnyMin;

	private Double btcESpotBtcCnyMax;

	private Double southxchangeSpotBtcUsd;

	private Double southxchangeSpotBtcUsdMin;

	private Double southxchangeSpotBtcUsdMax;

	private Double mtgoxSpotBtcSek;

	private Double mtgoxSpotBtcSekMin;

	private Double mtgoxSpotBtcSekMax;

	private Double abucoinsSpotBtcUsd;

	private Double abucoinsSpotBtcUsdMin;

	private Double abucoinsSpotBtcUsdMax;

	private Double yunbiSpotBtcCny;

	private Double yunbiSpotBtcCnyMin;

	private Double yunbiSpotBtcCnyMax;

	private Double coinsecureSpotBtcInr;

	private Double coinsecureSpotBtcInrMin;

	private Double coinsecureSpotBtcInrMax;

	private Double yobitSpotMvrBtc;

	private Double yobitSpotMvrBtcMin;

	private Double yobitSpotMvrBtcMax;

	private Double mtgoxSpotBtcChf;

	private Double mtgoxSpotBtcChfMin;

	private Double mtgoxSpotBtcChfMax;

	private Double dsxSpotBtcRub;

	private Double dsxSpotBtcRubMin;

	private Double dsxSpotBtcRubMax;

	private Double yobitSpotSdgBtc;

	private Double yobitSpotSdgBtcMin;

	private Double yobitSpotSdgBtcMax;

	private Double quoineSpotBtcInr;

	private Double quoineSpotBtcInrMin;

	private Double quoineSpotBtcInrMax;

	private Double quoineSpotBtcCny;

	private Double quoineSpotBtcCnyMin;

	private Double quoineSpotBtcCnyMax;

	private Double mtgoxSpotBtcRub;

	private Double mtgoxSpotBtcRubMin;

	private Double mtgoxSpotBtcRubMax;

	private Double mtgoxSpotBtcCny;

	private Double mtgoxSpotBtcCnyMin;

	private Double mtgoxSpotBtcCnyMax;

	private Double cexioSpotGhsBtc;

	private Double cexioSpotGhsBtcMin;

	private Double cexioSpotGhsBtcMax;

	private Double ccexSpotUsdtBtc;

	private Double ccexSpotUsdtBtcMin;

	private Double ccexSpotUsdtBtcMax;

	private Double mtgoxSpotBtcNzd;

	private Double mtgoxSpotBtcNzdMin;

	private Double mtgoxSpotBtcNzdMax;

	private Double yobitSpotPenBtc;

	private Double yobitSpotPenBtcMin;

	private Double yobitSpotPenBtcMax;

	private Double yobitSpotGhsBtc;

	private Double yobitSpotGhsBtcMin;

	private Double yobitSpotGhsBtcMax;

	private Double coinfloorSpotBtcPln;

	private Double coinfloorSpotBtcPlnMin;

	private Double coinfloorSpotBtcPlnMax;

	private Double mtgoxSpotBtcSgd;

	private Double mtgoxSpotBtcSgdMin;

	private Double mtgoxSpotBtcSgdMax;

	private Double dsxSpotBtcGbp;

	private Double dsxSpotBtcGbpMin;

	private Double dsxSpotBtcGbpMax;

	private Double yobitSpotMadBtc;

	private Double yobitSpotMadBtcMin;

	private Double yobitSpotMadBtcMax;

	private Double yobitSpotBamBtc;

	private Double yobitSpotBamBtcMin;

	private Double yobitSpotBamBtcMax;

	private Double mtgoxSpotBtcDkk;

	private Double mtgoxSpotBtcDkkMin;

	private Double mtgoxSpotBtcDkkMax;

	private Double southxchangeSpotBhdBtc;

	private Double southxchangeSpotBhdBtcMin;

	private Double southxchangeSpotBhdBtcMax;

	private Double bitlishSpotBtcEur;

	private Double bitlishSpotBtcEurMin;

	private Double bitlishSpotBtcEurMax;

	private Double coingiSpotBtcUsd;

	private Double coingiSpotBtcUsdMin;

	private Double coingiSpotBtcUsdMax;

	private Double coinfloorSpotBtcEur;

	private Double coinfloorSpotBtcEurMin;

	private Double coinfloorSpotBtcEurMax;

	private Double bitlishSpotBtcUsd;

	private Double bitlishSpotBtcUsdMin;

	private Double bitlishSpotBtcUsdMax;

	private Double bitlishSpotBtcRub;

	private Double bitlishSpotBtcRubMin;

	private Double bitlishSpotBtcRubMax;

	private Double mtgoxSpotBtcThb;

	private Double mtgoxSpotBtcThbMin;

	private Double mtgoxSpotBtcThbMax;

	private Double coingiSpotBtcEur;

	private Double coingiSpotBtcEurMin;

	private Double coingiSpotBtcEurMax;

	private Double vbtcSpotBtcClp;

	private Double vbtcSpotBtcClpMin;

	private Double vbtcSpotBtcClpMax;

	private Double vbtcSpotBtcVef;

	private Double vbtcSpotBtcVefMin;

	private Double vbtcSpotBtcVefMax;

	private Double vbtcSpotBtcVnd;

	private Double vbtcSpotBtcVndMin;

	private Double vbtcSpotBtcVndMax;

	private Double vbtcSpotBtcBrl;

	private Double vbtcSpotBtcBrlMin;

	private Double vbtcSpotBtcBrlMax;

	private Double vbtcSpotBtcPkr;

	private Double vbtcSpotBtcPkrMin;

	private Double vbtcSpotBtcPkrMax;

	private Double foxbitSpotBtcBrl;

	private Double foxbitSpotBtcBrlMin;

	private Double foxbitSpotBtcBrlMax;

	private Double surbitcoinSpotBtcVef;

	private Double surbitcoinSpotBtcVefMin;

	private Double surbitcoinSpotBtcVefMax;

	private Double krakenSpotBtcKrw;

	private Double krakenSpotBtcKrwMin;

	private Double krakenSpotBtcKrwMax;

	private Double bitbaySpotBtcUsd;

	private Double bitbaySpotBtcUsdMin;

	private Double bitbaySpotBtcUsdMax;

	private Double bitbaySpotBtcEur;

	private Double bitbaySpotBtcEurMin;

	private Double bitbaySpotBtcEurMax;

	private Double acxSpotBtcUsd;

	private Double acxSpotBtcUsdMin;

	private Double acxSpotBtcUsdMax;

	private Double bitbaySpotBtcPln;

	private Double bitbaySpotBtcPlnMin;

	private Double bitbaySpotBtcPlnMax;

	private Double bterSpotBtcUsd;

	private Double bterSpotBtcUsdMin;

	private Double bterSpotBtcUsdMax;

	private Double lakebtcSpotBtcHkd;

	private Double lakebtcSpotBtcHkdMin;

	private Double lakebtcSpotBtcHkdMax;

	private Double xbtceSpotBtcRub;

	private Double xbtceSpotBtcRubMin;

	private Double xbtceSpotBtcRubMax;

	private Double xbtceSpotBtcJpy;

	private Double xbtceSpotBtcJpyMin;

	private Double xbtceSpotBtcJpyMax;

	private Double xbtceSpotBtcUsd;

	private Double xbtceSpotBtcUsdMin;

	private Double xbtceSpotBtcUsdMax;

	private Double xbtceSpotBtcEur;

	private Double xbtceSpotBtcEurMin;

	private Double xbtceSpotBtcEurMax;

	private Double xbtceSpotBtcGbp;

	private Double xbtceSpotBtcGbpMin;

	private Double xbtceSpotBtcGbpMax;

	private Double lakebtcSpotBtcSek;

	private Double lakebtcSpotBtcSekMin;

	private Double lakebtcSpotBtcSekMax;

	private Double lakebtcSpotBtcKrw;

	private Double lakebtcSpotBtcKrwMin;

	private Double lakebtcSpotBtcKrwMax;

	private Double cryptopiaSpotEtbBtc;

	private Double cryptopiaSpotEtbBtcMin;

	private Double cryptopiaSpotEtbBtcMax;

	private Double xbtceSpotBtcInr;

	private Double xbtceSpotBtcInrMin;

	private Double xbtceSpotBtcInrMax;

	private Double anxproSpotBtcUsd;

	private Double anxproSpotBtcUsdMin;

	private Double anxproSpotBtcUsdMax;

	private Double anxproSpotBtcAud;

	private Double anxproSpotBtcAudMin;

	private Double anxproSpotBtcAudMax;

	private Double anxproSpotBtcCad;

	private Double anxproSpotBtcCadMin;

	private Double anxproSpotBtcCadMax;

	private Double anxproSpotBtcEur;

	private Double anxproSpotBtcEurMin;

	private Double anxproSpotBtcEurMax;

	private Double lykkeSpotBtcChf;

	private Double lykkeSpotBtcChfMin;

	private Double lykkeSpotBtcChfMax;

	private Double lykkeSpotBtcEur;

	private Double lykkeSpotBtcEurMin;

	private Double lykkeSpotBtcEurMax;

	private Double anxproSpotBtcGbp;

	private Double anxproSpotBtcGbpMin;

	private Double anxproSpotBtcGbpMax;

	private Double lykkeSpotBtcGbp;

	private Double lykkeSpotBtcGbpMin;

	private Double lykkeSpotBtcGbpMax;

	private Double anxproSpotBtcHkd;

	private Double anxproSpotBtcHkdMin;

	private Double anxproSpotBtcHkdMax;

	private Double lykkeSpotBtcJpy;

	private Double lykkeSpotBtcJpyMin;

	private Double lykkeSpotBtcJpyMax;

	private Double anxproSpotBtcJpy;

	private Double anxproSpotBtcJpyMin;

	private Double anxproSpotBtcJpyMax;

	private Double anxproSpotBtcNzd;

	private Double anxproSpotBtcNzdMin;

	private Double anxproSpotBtcNzdMax;

	private Double lykkeSpotBtcUsd;

	private Double lykkeSpotBtcUsdMin;

	private Double lykkeSpotBtcUsdMax;

	private Double anxproSpotBtcSgd;

	private Double anxproSpotBtcSgdMin;

	private Double anxproSpotBtcSgdMax;

	private Double coinexchangeSpotRubBtc;

	private Double coinexchangeSpotRubBtcMin;

	private Double coinexchangeSpotRubBtcMax;

	private Double bitfinexSpotBtcGbp;

	private Double bitfinexSpotBtcGbpMin;

	private Double bitfinexSpotBtcGbpMax;

	private Double bitfinexSpotBtcJpy;

	private Double bitfinexSpotBtcJpyMin;

	private Double bitfinexSpotBtcJpyMax;

	private Double yobitSpotUsdtBtc;

	private Double yobitSpotUsdtBtcMin;

	private Double yobitSpotUsdtBtcMax;

	private Double southxchangeSpotBtcTusd;

	private Double southxchangeSpotBtcTusdMin;

	private Double southxchangeSpotBtcTusdMax;

	private Double braziliexSpotUsdtBtc;

	private Double braziliexSpotUsdtBtcMin;

	private Double braziliexSpotUsdtBtcMax;

	private Double braziliexSpotBtcUsdt;

	private Double braziliexSpotBtcUsdtMin;

	private Double braziliexSpotBtcUsdtMax;

	private Double bleutradeSpotUsdtBtc;

	private Double bleutradeSpotUsdtBtcMin;

	private Double bleutradeSpotUsdtBtcMax;

	private Double bleutradeSpotBtcUsdt;

	private Double bleutradeSpotBtcUsdtMin;

	private Double bleutradeSpotBtcUsdtMax;

	private Double binanceSpotTusdBtc;

	private Double binanceSpotTusdBtcMin;

	private Double binanceSpotTusdBtcMax;

	private Double cryptopiaSpotTusdBtc;

	private Double cryptopiaSpotTusdBtcMin;

	private Double cryptopiaSpotTusdBtcMax;

	private Double binanceSpotBtcTusd;

	private Double binanceSpotBtcTusdMin;

	private Double binanceSpotBtcTusdMax;

	private Double bittrexSpotBtcUsd;

	private Double bittrexSpotBtcUsdMin;

	private Double bittrexSpotBtcUsdMax;

	private Double livecoinSpotPlnBtc;

	private Double livecoinSpotPlnBtcMin;

	private Double livecoinSpotPlnBtcMax;

	private Double bitsoSpotTusdBtc;

	private Double bitsoSpotTusdBtcMin;

	private Double bitsoSpotTusdBtcMax;

	private Double dsxSpotBtcUsdt;

	private Double dsxSpotBtcUsdtMin;

	private Double dsxSpotBtcUsdtMax;

	private Double yobitSpotTusdBtc;

	private Double yobitSpotTusdBtcMin;

	private Double yobitSpotTusdBtcMax;

	private Double hitbtcSpotBtcTusd;

	private Double hitbtcSpotBtcTusdMin;

	private Double hitbtcSpotBtcTusdMax;

	private Double wexnzSpotBtcUsdt;

	private Double wexnzSpotBtcUsdtMin;

	private Double wexnzSpotBtcUsdtMax;

	private Double acxSpotBtcUsdt;

	private Double acxSpotBtcUsdtMin;

	private Double acxSpotBtcUsdtMax;

	private Double upbitSpotTusdBtc;

	private Double upbitSpotTusdBtcMin;

	private Double upbitSpotTusdBtcMax;

	private Double upbitSpotBtcKrw;

	private Double upbitSpotBtcKrwMin;

	private Double upbitSpotBtcKrwMax;

	private Double upbitSpotBtcUsdt;

	private Double upbitSpotBtcUsdtMin;

	private Double upbitSpotBtcUsdtMax;

	private Double bitlishSpotBtcGbp;

	private Double bitlishSpotBtcGbpMin;

	private Double bitlishSpotBtcGbpMax;

	private Double bitlishSpotBtcJpy;

	private Double bitlishSpotBtcJpyMin;

	private Double bitlishSpotBtcJpyMax;

	private Double bitlishSpotBtcUsdt;

	private Double bitlishSpotBtcUsdtMin;

	private Double bitlishSpotBtcUsdtMax;

	private Double bitbankSpotBtcJpy;

	private Double bitbankSpotBtcJpyMin;

	private Double bitbankSpotBtcJpyMax;

	private Double bitzSpotBtcUsdt;

	private Double bitzSpotBtcUsdtMin;

	private Double bitzSpotBtcUsdtMax;

	private Double exmoSpotBtcTry;

	private Double exmoSpotBtcTryMin;

	private Double exmoSpotBtcTryMax;

	public VolumenBTC(Timedata td) {
		super(td);
	}
	
	public VolumenBTC(Instant time_period_start, Instant time_period_end, double volume_traded) {
		setTime_period_start(time_period_start);
		setTime_period_end(time_period_end);
		setVolume_traded(volume_traded);
	}
}
