package com.criptovolumen.entidades.periodo;

public enum TipoPeriodo {

     _30MIN,
     _1HRS,
     _2HRS,
     _3HRS,
     _4HRS,
     _6HRS,
     _8HRS,
     _12HRS,
     _1DAY,
     _2DAY,
     _3DAY,
     _5DAY,
     _7DAY,
     _10DAY,
     _1MTH,
     _2MTH,
     _3MTH,
     _4MTH,
     _6MTH,
     _1YRS,
     _2YRS,
     _3YRS,
     _4YRS,
     _5YRS;
}
